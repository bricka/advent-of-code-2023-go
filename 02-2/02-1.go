package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

type cubeSet struct {
	red uint64
	blue uint64
	green uint64
}

func (s cubeSet) getPower() uint64 {
	return s.red * s.blue * s.green
}

type game struct {
	id uint64
	allPulled []cubeSet
}

func (g game) getMinimumCubeSet() cubeSet {
	red := uint64(0)
	green := uint64(0)
	blue := uint64(0)

	for _, s := range g.allPulled {
		if s.red > red {
			red = s.red
		}
		if s.green > green {
			green = s.green
		}
		if s.blue > blue {
			blue = s.blue
		}
	}

	return cubeSet{
		red:red,
		green:green,
		blue:blue,
	}
}

func getGameId(s string) uint64 {
	_, idString, _ := strings.Cut(s, " ")
	id, err := strconv.ParseUint(idString, 10, 0)
	checkErr(err)
	return id
}

func getPulledSets(s string) []cubeSet {
	pulls := strings.Split(s, "; ")

	allPulled := []cubeSet{}

	for _, pull := range pulls {
		red := uint64(0)
		green := uint64(0)
		blue := uint64(0)

		colorsPulled := strings.Split(pull, ", ")
		for _, colorPulled := range colorsPulled {
			countString, color, _ := strings.Cut(colorPulled, " ")
			count, err := strconv.ParseUint(countString, 10, 0)
			checkErr(err)

			switch(color) {
			case "red": red = count
			case "green": green = count
			case "blue": blue = count
			default: panic(color)
			}
		}

		allPulled = append(allPulled, cubeSet{
			red:red,
			green:green,
			blue:blue,
		})
	}

	return allPulled
}

func readInput() ([]game) {
	inputFile := os.Args[1]
	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	games := []game{}

	for scanner.Scan() {
		line := scanner.Text()
		gameString, pulledString, _ := strings.Cut(line, ": ")
		id := getGameId(gameString)
		pulledSets := getPulledSets(pulledString)
		games = append(games, game{
			id:id,
			allPulled:pulledSets,
		})
	}

	return games
}

func main() {
	games := readInput()

	powerSum := uint64(0)

	for _, game := range games {
		powerSum += game.getMinimumCubeSet().getPower()
	}

	fmt.Println(powerSum)
}
