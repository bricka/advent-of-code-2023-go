package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func getFirstDigit(line string) (string, error) {
	for _, char := range line {
		if char >= 48 && char <= 57 {
			return string(char), nil
		}
	}

	return "", fmt.Errorf("No digit found in string %v", line)
}

func getLastDigit(line string) (string, error) {
	for i := len(line)-1; i >= 0; i-- {
		char := line[i]
		if char >= 48 && char <= 57 {
			return string(char), nil
		}
	}

	return "", fmt.Errorf("No digit found in string %v", line)
}

func main() {
	inputFile := os.Args[1]
	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	sum := uint64(0)

	for scanner.Scan() {
		line := scanner.Text()
		fmt.Printf("Line is: %v\n", line)
		firstDigit, err := getFirstDigit(line)
		if err != nil {
			log.Fatal(err)
		}
		lastDigit, err := getLastDigit(line)
		if err != nil {
			log.Fatal(err)
		}
		numString := firstDigit + lastDigit
		log.Printf("Number String is %v", numString)
		num, err := strconv.ParseUint(numString, 10, 0)
		if err != nil {
			log.Fatal(err)
		}

		sum += num
	}

	fmt.Println(sum)
}
