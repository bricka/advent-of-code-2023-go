package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
)


func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

func convertDigit(digit string) string {
	converted := digit

	switch(digit) {
	case "one": converted = "1"
	case "two": converted = "2"
	case "three": converted = "3"
	case "four": converted = "4"
	case "five": converted = "5"
	case "six": converted = "6"
	case "seven": converted = "7"
	case "eight": converted = "8"
	case "nine": converted = "9"
	}

	return converted
}

func getFirstDigit(line string) (string, error) {
	digitRegexp := regexp.MustCompile("\\d|one|two|three|four|five|six|seven|eight|nine")

	match := digitRegexp.FindString(line)
	if match == "" {
		return "", fmt.Errorf("No digit found in string %v", line)
	}

	converted := match

	switch(match) {
	case "one": converted = "1"
	case "two": converted = "2"
	case "three": converted = "3"
	case "four": converted = "4"
	case "five": converted = "5"
	case "six": converted = "6"
	case "seven": converted = "7"
	case "eight": converted = "8"
	case "nine": converted = "9"
	}

	return converted, nil
}

func reverseString (s string) string {
	reversed := ""

	for _, r := range s {
		reversed = string(r) + reversed
	}

	return reversed
}

func getLastDigit(line string) (string, error) {
	// We need to reverse the string and reverse each word

	digitRegexp := regexp.MustCompile("\\d|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin")

	match := digitRegexp.FindString(reverseString(line))
	if match == "" {
		return "", fmt.Errorf("No digit found in string %v", line)
	}

	converted := match
	switch(match) {
	case "eno": converted = "1"
	case "owt": converted = "2"
	case "eerht": converted = "3"
	case "ruof": converted = "4"
	case "evif": converted = "5"
	case "xis": converted = "6"
	case "neves": converted = "7"
	case "thgie": converted = "8"
	case "enin": converted = "9"
	}

	return converted, nil
}

func main() {
	inputFile := os.Args[1]
	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	sum := uint64(0)

	for scanner.Scan() {
		line := scanner.Text()
		log.Printf("Line is: %v\n", line)
		firstDigit, err := getFirstDigit(line)
		checkErr(err)
		lastDigit, err := getLastDigit(line)
		numString := firstDigit + lastDigit
		log.Printf("Number String is %v", numString)
		num, err := strconv.ParseUint(numString, 10, 0)
		checkErr(err)

		sum += num
	}

	fmt.Println(sum)
}
