package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

type pulled struct {
	red uint64
	blue uint64
	green uint64
}

type game struct {
	id uint64
	allPulled []pulled
}

func (g game) isValidFor(red uint64, green uint64, blue uint64) bool {
	for _, pulled := range g.allPulled {
		if pulled.red > red || pulled.green > green || pulled.blue > blue {
			return false
		}
	}

	return true
}

func getGameId(s string) uint64 {
	_, idString, _ := strings.Cut(s, " ")
	id, err := strconv.ParseUint(idString, 10, 0)
	checkErr(err)
	return id
}

func getPulledSets(s string) []pulled {
	pulls := strings.Split(s, "; ")

	allPulled := []pulled{}

	for _, pull := range pulls {
		red := uint64(0)
		green := uint64(0)
		blue := uint64(0)

		colorsPulled := strings.Split(pull, ", ")
		for _, colorPulled := range colorsPulled {
			countString, color, _ := strings.Cut(colorPulled, " ")
			count, err := strconv.ParseUint(countString, 10, 0)
			checkErr(err)

			switch(color) {
			case "red": red = count
			case "green": green = count
			case "blue": blue = count
			default: panic(color)
			}
		}

		allPulled = append(allPulled, pulled{
			red:red,
			green:green,
			blue:blue,
		})
	}

	return allPulled
}

func readInput() ([]game) {
	inputFile := os.Args[1]
	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	games := []game{}

	for scanner.Scan() {
		line := scanner.Text()
		gameString, pulledString, _ := strings.Cut(line, ": ")
		id := getGameId(gameString)
		pulledSets := getPulledSets(pulledString)
		games = append(games, game{
			id:id,
			allPulled:pulledSets,
		})
	}

	return games
}

func main() {
	games := readInput()

	validGameSum := uint64(0)

	for _, game := range games {
		if game.isValidFor(12, 13, 14) {
			validGameSum += game.id
		}
	}

	fmt.Println(validGameSum)
}
