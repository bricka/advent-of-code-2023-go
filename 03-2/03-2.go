package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"

	mapset "github.com/deckarep/golang-set/v2"
)

func checkErr(e error) {
	if e != nil {
		log.Fatal(e)
	}
}

type number struct {
	num int
	lineNumber int
	startPosInclusive int
	endPosInclusive int
}

func (n number) getAdjacentCoordinates() []position {
	coordinates := []position{}

	relevantLineNumbers := []int{n.lineNumber + 1}

	if n.lineNumber > 0 {
		relevantLineNumbers = append(relevantLineNumbers, n.lineNumber - 1)
	}

	// First everything directly above and below

	for i := n.startPosInclusive; i <= n.endPosInclusive; i++ {
		for _, ln := range relevantLineNumbers {
			coordinates = append(coordinates, newPosition(ln, i))
		}
	}

	// Then left and right
	relevantLineNumbers = append(relevantLineNumbers, n.lineNumber)

	columnsLeftAndRight := []int{n.endPosInclusive + 1}
	if n.startPosInclusive > 0 {
		columnsLeftAndRight = append(columnsLeftAndRight, n.startPosInclusive - 1)
	}

	for _, col := range columnsLeftAndRight {
		for _, ln := range relevantLineNumbers {
			coordinates = append(coordinates, newPosition(ln, col))
		}
	}

	return coordinates
}

func (n number) isPositionAdjacent(p position) bool {
	for _, adjacentCoord := range n.getAdjacentCoordinates() {
		if adjacentCoord == p {
			return true
		}
	}

	return false
}

type position struct {
	line int
	col int
}

func newPosition(line int, col int) position {
	return position{
		line:line,
		col:col,
	}
}

func (p1 position) Compare(p2 position) int {
	if p1.line != p2.line {
		return p1.line - p2.line
	}

	return p1.col - p2.col
}

type symbolMap struct {
	symbolPositions mapset.Set[position]
}

func (s symbolMap) addSymbol(p position) {
	s.symbolPositions.Add(p)
}

func (s symbolMap) containsCoord(p position) bool {
	return s.symbolPositions.Contains(p)
}

func readInput() ([]number, symbolMap, []position) {
	inputFile := os.Args[1]
	f, err := os.Open(inputFile)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	numbers := []number{}
	symbols := symbolMap{
		symbolPositions: mapset.NewSet[position](),
	}
	stars := []position{}

	numRegex := regexp.MustCompile("\\d+")
	symbolRegex := regexp.MustCompile("[^\\d.]")
	starRegex := regexp.MustCompile("\\*")

	lineNum := 0

	for scanner.Scan() {
		line := scanner.Text()

		numberMatches := numRegex.FindAllStringIndex(line, -1)

		for _, numberMatch := range numberMatches {
			num, err := strconv.Atoi(line[numberMatch[0]:numberMatch[1]])
			checkErr(err)
			numbers = append(numbers, number{
				num: num,
				lineNumber: lineNum,
				startPosInclusive: numberMatch[0],
				endPosInclusive: numberMatch[1] - 1,
			})
		}

		symbolMatches := symbolRegex.FindAllStringIndex(line, -1)

		for _, symbolMatch := range symbolMatches {
			symbols.addSymbol(newPosition(lineNum, symbolMatch[0]))
		}

		starMatches := starRegex.FindAllStringIndex(line, -1)
		for _, starMatch := range starMatches {
			stars = append(stars, newPosition(lineNum, starMatch[0]))
		}

		lineNum++
	}

	return numbers, symbols, stars
}

func getPartNumbers(numbers []number, symbols symbolMap) []number {
	partNumbers := []number{}

perNum:
	for _, num := range numbers {
		adjacentCoords := num.getAdjacentCoordinates()

		for _, adjacentCoord := range adjacentCoords {
			if symbols.containsCoord(adjacentCoord) {
				partNumbers = append(partNumbers, num)
				continue perNum
			}
		}
	}

	return partNumbers
}

func findGearRatios(partNumbers []number, starPositions []position) []int {
	gearRatios := []int{}

	for _, starPosition := range starPositions {
		adjacentPartNumbers := []int{}

		for _, partNumber := range partNumbers {
			if partNumber.isPositionAdjacent(starPosition) {
				adjacentPartNumbers = append(adjacentPartNumbers, partNumber.num)
			}
		}

		if len(adjacentPartNumbers) == 2 {
			gearRatios = append(gearRatios, adjacentPartNumbers[0] * adjacentPartNumbers[1])
		}
	}

	return gearRatios
}

func main() {
	numbers, symbols, stars := readInput()
	fmt.Println(numbers)

	partNumbers := getPartNumbers(numbers, symbols)
	gearRatios := findGearRatios(partNumbers, stars)

	sum := 0

	for _, gearRatio := range gearRatios {
		sum += gearRatio
	}

	fmt.Println(sum)
}
